﻿using System;

namespace pognali
{

    class Depositor
    {
        public bool ChecksDepositorAge(int age)
        {
            if (age < 18) //
            {
                return false;
            }
            else
            {
                return true;
            }
        }
    }
    
    class Deposit
    {
        public double CalculateFinalDeposit(double userCash, int yearsOfDeposit)
        {
            for (int i = 0; i < yearsOfDeposit; i++)
            {
                var yearPercent = userCash * 0.04;
                userCash += yearPercent;
                userCash = Math.Round(userCash, 2, MidpointRounding.ToEven);
            }
            return userCash;
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            var Depositor = new Depositor();

            while (true)
            {
                Console.WriteLine("Enter your age:");
                var ageString = Console.ReadLine();
                var isAgeStringValid = int.TryParse(ageString, out int age);

                if (isAgeStringValid)
                {
                    var result = Depositor.ChecksDepositorAge(age);

                    if (result)
                        {
                        Console.WriteLine("You can make a deposit \r\n");
                        }
                    else
                        {
                        Console.WriteLine($"You can't make a deposit, please, wait: {18 - age} years \r\n");
                        return;
                        }
                }
                else
                {
                    Console.WriteLine("Input error, please, try again ");
                }

                Console.WriteLine("Enter deposit amount:");
                var userDepositAmountString = Console.ReadLine();
                var userDepositAmount = double.Parse(userDepositAmountString);

                Console.WriteLine("Enter deposit tern:");
                var depositDurationString = Console.ReadLine();
                var depositDuration = int.Parse(depositDurationString);

                if (Depositor.ChecksDepositorAge(age))
                {
                    var finalDeposit = new Deposit().CalculateFinalDeposit(userDepositAmount, depositDuration);
                    Console.WriteLine($"After { depositDuration } year, you deposit will be: { finalDeposit } \r\n");
                }
            }
        }
    }
}
